const mongoose = require('mongoose')
const { Schema } = mongoose;

const studentSchema = Schema({
    lastName: String,
    firstName: String,
    email: {
        type: String,
        required: true
    },
    age: Number
});

const studentModel = mongoose.model('Student', studentSchema);
module.exports = studentModel;

// const students = [

//     {
//         id: 1,
//         name: "Achile",
//         firstName: "Achi",
//         age: 12,
//         email: "achil@gmail.com"
//     },
//     {
//         id: 2,
//         name: "Achile1",
//         firstName: "Achi",
//         age: 12,
//         email: "achil@gmail.com"
//     },
//     {
//         id: 3,
//         name: "Achile2",
//         firstName: "Achi",
//         age: 12,
//         email: "achil@gmail.com"
//     },
//     {
//         id: 4,
//         name: "Achile3",
//         firstName: "Achi",
//         age: 12,
//         email: "achil@gmail.com"
//     }
// ]

// module.exports = students;